import React, { useState, createContext, useContext } from 'react';
import './App.css';

const Verse = ({value}) => {
  const api = useContext(GKAppApi);

  return (
    <label>
      <textarea value={value} onChange={event => { api.setVerse(event.target.value) }} />
    </label>
  );

}

export const Switchboard = ({ value }) => {
  const api = useContext(GKAppApi);
  api.setVerse(value);
};

// export function test_function(param) { console.log(`hare krsna : ${param}`); }

const Loaded = ({params}) =>
  (<div>
    <p>1. value = { typeof(params.value) } , wasm? = {(params.wasm)?"true":"false"}</p>
    <button onClick={params.wasm.gurave}>om ajñāna timirāndhasya</button>
    <button onClick={params.wasm.nityanandakhya}>saṅkarṣaṇaḥ kāraṇa-toya-śāyī</button>
    <button onClick={params.wasm.sacinandanah}>anarpita-carīṁ cirāt</button>
    <hr/>
    <Verse value={ params.value }/>
    </div>
  )

const Unloaded = ({ loading, loadWasm }) => {
    return loading ? (
      <div>Loading...</div>
    ) : (
      <button onClick={loadWasm}>Load library</button>
    );
};

const GKAppApi = createContext(null);

const App = () => {
  const [loading, setLoading] = useState(false);
  const [wasm, setWasm] = useState(null);
  const [verse, setVerse] = useState("śloka");

  const api = { setVerse };

  const loadWasm = async () => {
    try {
      setLoading(true);
      const wasm = await import ('react_rust_wasm');
      setWasm(wasm);
      const verse = "śloka";
      setVerse(verse);
    } finally {
      setLoading(false);
    }
  };
  return (
    <div className="App">
      <header className="App-header">
        {wasm ? (
          <GKAppApi.Provider value={api}>
            <p>0. value = {verse}</p>
            <Loaded params={ {wasm:wasm, value:verse} }/>
          </GKAppApi.Provider>
        ) : (
          <Unloaded loading={loading} loadWasm={loadWasm} />
        )}
      </header>
    </div>
  );
}

export default App;
