# GKapp
(Gaudiya Kirtan app)

## installation
All developers will need to run the [wasm-pack installer](https://rustwasm.github.io/wasm-pack/installer/)
And probably this too: [rust installer](https://www.rust-lang.org/tools/install)

---
## pre-compile
```bash
rustup default stable
rustup target add wasm32-unknown-unknown
# cargo install wasm-bindgen-cli
cargo install wasm-bindgen-cli --force
cargo build --target wasm32-unknown-unknown
# you may have to delete everything but "dependencies"
# or maybe "devDependencies" from package.json before wasm-pack build
wasm-pack build --dev
wasm-bindgen target/wasm32-unknown-unknown/debug/react_rust_wasm.wasm --out-dir build

# ---------------------------------------WEB
# restore package.json before you run yarn
yarn start

# ---------------------------------------IOS
cd ios
# you may have to comment out fishhook from Podfile to get this to compile...
# but of course we don't want that long term.
pod install
cd ..
react-native run-ios

# ---------------------------------------ANDROID
react-native run-android
```

next steps
1. rust shall call react hooks
  * fill in text field
  * .

2. ios and android rust<-->react bridge
  * get rust working on each target
  * then bridge
