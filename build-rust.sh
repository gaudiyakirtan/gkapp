if [[ -f "package-dependencies-only.json" ]] ; then
  mv package.json __package.json__
  mv package-dependencies-only.json package.json
fi

yarn upgrade react_rust_wasm
cargo build --target wasm32-unknown-unknown
wasm-pack build
wasm-bindgen target/wasm32-unknown-unknown/release/react_rust_wasm.wasm --out-dir build

if [[ -f "__package.json__" ]] ; then
  mv package.json package-dependencies-only.json
  mv __package.json__ package.json
fi

yarn
echo -n "continue [y/n]? " ; read yn ; if [[ $yn = "y" ]] ; then
  yarn start ; fi
